<?php

namespace WooTkpEFTGateway;

use WC_Admin_Settings;
use WC_Payment_Gateway;
use WooTkpEFTGateway\config\Settings;
use WooTkpEFTGateway\GatewayInterface;
use WooTkpEFTGateway\utils\HelpersTrait;
use WP_Error;

defined('ABSPATH') or exit;
defined('WPINC') or die;


/**
 * Gateway
 *
 * This class provides holds the Gateway functionality
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Gateway extends WC_Payment_Gateway
{

    use HelpersTrait;

    /**
     * API credentials
     *
     * @var string
     */
    public $username;
    public $password;

    /**
     * Logging enabled?
     *
     * @var bool
     */
    public $logging;

    /**
     * URL for IPN
     *
     * @var null|string
     */
    public $notifyUrl = null;

    /**
     * @var Reference to logging class.
     */
    //private static $log;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = GatewayInterface::PLUGIN_ID;
        $this->method_title = __(GatewayInterface::PLUGIN_TITLE, GatewayInterface::PLUGIN_ID);
        $this->method_description = __(GatewayInterface::PLUGIN_TITLE . ' allows your customers to pay via EFT.', GatewayInterface::PLUGIN_ID);
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        // Get setting values.
        $this->icon = plugin_dir_url( __FILE__ )."../assets/echeck2.svg";
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->username = $this->get_option('gw_api_username');
        $this->password = $this->get_option('gw_api_password');

        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
            add_action('admin_notices', [$this, 'admin_notices']);
        }
    }

    /**
     * Initialize Gateway Settings Form Fields
     */
    public function init_form_fields()
    {
        $this->form_fields = Settings::getSettings();
    }

    /**
     * Payment form on checkout page
     */
    public function payment_fields()
    {
        $fields = [
            'js' =>'
                <script>
                    transit = document.getElementById("' . $this->id . '-transit-no")
                    transit.onkeyup = function(e){
                        if(transit.value<0){transit.value=0}
                        if(transit.value>99999){transit.value=99999}
                    }
                    transit.onkeypress = function(e){
                        if(transit.value<0){transit.value=0}
                        if(transit.value>99999){transit.value=99999}
                    }
                    account = document.getElementById("' . $this->id . '-account-no")
                    account.onkeyup = function(e){
                        if(account.value<0){account.value=0}
                        if(account.value>99999999999999999999){account.value=99999999999999999999}
                    }
                    account.onkeypress = function(e){
                        if(account.value<0){account.value=0}
                        if(account.value>99999999999999999999){account.value=99999999999999999999}
                    }
                </script>
            ',
            'style' =>'
                <style>
                /* Chrome, Safari, Edge, Opera */
                input::-webkit-outer-spin-button,
                input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
                }

                /* Firefox */
                input[type=number] {
                -moz-appearance: textfield;
                }
                </style>
            ',
            'institution-code' => '<p class="form-row form-row-wide">
			<label for="' . $this->id . '-institution-code">' . __("Institution Code", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
                <select style="
                        font-size: 1.5em;
                        padding: 8px;
                        background-repeat: no-repeat;
                        background-position: right .618em center;
                        background-size: 32px 20px;
                        border-color: #c7c1c6;
                        border-top-color: #bbb3b9;
                        width: 100%;
                        background: #fff;
                    "
                    id="' . $this->id . '-institution-code" name="' . $this->id . '-institution-code">
                    <option value="">Select Institution</option>
                    <option value="830" style="width:146px">Airline Financial Credit Union Limited</option>
                    <option value="219" style="width:146px">Alberta Treasury Branches</option>
                    <option value="810" style="width:146px">All Trans Financial Services Credit Union Limited</option>
                    <option value="890" style="width:146px">Alliance des caisses populaires de l\'Ontario Limitée</option>
                    <option value="842" style="width:146px">Alterna Savings and Credit Union</option>
                    <option value="303" style="width:146px">Amex Bank of Canada</option>
                    <option value="808" style="width:146px">Arnstein Community Credit Union Limited</option>
                    <option value="839" style="width:146px">Atlantic Central</option>
                    <option value="618" style="width:146px">B2B Bank (formerly B2B Trust)</option>
                    <option value="241" style="width:146px">Bank of America - National Association</option>
                    <option value="177" style="width:146px">Bank of Canada</option>
                    <option value="308" style="width:146px">Bank of China (Canada)</option>
                    <option value="001" style="width:146px">Bank of Montreal</option>
                    <option value="245" style="width:146px">Bank of Tokyo-Mitsubishi UFJ (Canada)</option>
                    <option value="342" style="width:146px">Bank West</option>
                    <option value="360" style="width:146px">Barclay\'s Bank PLC Canada Branch</option>
                    <option value="250" style="width:146px">BNP Paribas (Canada)</option>
                    <option value="311" style="width:146px">BofA Canada Bank</option>
                    <option value="347" style="width:146px">Bridgewater Bank</option>
                    <option value="010" style="width:146px">Canadian Imperial Bank of Commerce</option>
                    <option value="338" style="width:146px">Canadian Tire Bank</option>
                    <option value="030" style="width:146px">Canadian Western Bank</option>
                    <option value="323" style="width:146px">Capital One Bank (Canada Branch)</option>
                    <option value="809" style="width:146px">Central 1 Credit Union British Columbia</option>
                    <option value="828" style="width:146px">Central 1 Credit Union Ontario</option>
                    <option value="548" style="width:146px">CIBC Trust Corporation</option>
                    <option value="260" style="width:146px">Citibank Canada</option>
                    <option value="328" style="width:146px">Citibank N.A.</option>
                    <option value="309" style="width:146px">Citizens Bank of Canada</option>
                    <option value="330" style="width:146px">Comerica Bank</option>
                    <option value="807" style="width:146px">Communication Technologies Credit Union Limited</option>
                    <option value="507" style="width:146px">Community Trust Company</option>
                    <option value="853" style="width:146px">Concentra Financial Services Association</option>
                    <option value="899" style="width:146px">Credit Union Central Alberta Limited</option>
                    <option value="879" style="width:146px">Credit Union Central of Manitoba Limited</option>
                    <option value="889" style="width:146px">Credit Union Central of Saskatchewan</option>
                    <option value="608" style="width:146px">CS Alterna Bank</option>
                    <option value="315" style="width:146px">CTC Bank of Canada</option>
                    <option value="265" style="width:146px">Deutsche Bank AG</option>
                    <option value="352" style="width:146px">DirectCash Bank</option>
                    <option value="840" style="width:146px">Dundalk District Credit Union Limited</option>
                    <option value="343" style="width:146px">Dundee Bank of Canada</option>
                    <option value="532" style="width:146px">Effort Trust Company</option>
                    <option value="345" style="width:146px">Fifth Third Bank</option>
                    <option value="332" style="width:146px">First Commercial Bank</option>
                    <option value="310" style="width:146px">First Nations Bank of Canada</option>
                    <option value="344" style="width:146px">General Bank of Canada</option>
                    <option value="844" style="width:146px">Goderich Community Credit Union Limited</option>
                    <option value="854" style="width:146px">Golden Horseshoe Credit Union Limited</option>
                    <option value="321" style="width:146px">Habib Canadian Bank</option>
                    <option value="358" style="width:146px">HomEquity Bank</option>
                    <option value="630" style="width:146px">Household Trust Company</option>
                    <option value="016" style="width:146px">HSBC Bank Canada</option>
                    <option value="333" style="width:146px">HSBC Bank USA National Association</option>
                    <option value="604" style="width:146px">HSBC Mortgage Corporation (Canada)</option>
                    <option value="340" style="width:146px">ICICI Bank Canada</option>
                    <option value="625" style="width:146px">Industrial Alliance Trust Inc.</option>
                    <option value="307" style="width:146px">Industrial and Commercial Bank of China (Canada)</option>
                    <option value="614" style="width:146px">ING Bank of Canada</option>
                    <option value="536" style="width:146px">Investors Group Trust Co. Ltd.</option>
                    <option value="314" style="width:146px">J.P. Morgan Bank Canada</option>
                    <option value="354" style="width:146px">Jameson Bank</option>
                    <option value="270" style="width:146px">JPMorgan Chase Bank National Association</option>
                    <option value="275" style="width:146px">Korea Exchange Bank of Canada</option>
                    <option value="815" style="width:146px">La Confédération des Caisses Populaires et D’Économie Desjardins du Québec</option>
                    <option value="865" style="width:146px">La Fédération des Caisses Populaires Acadiennes Limitée   </option>
                    <option value="829" style="width:146px">La Fédération des Caisses Populaires de l’Ontario Inc.</option>
                    <option value="819" style="width:146px">La Fédération des caisses populaires du Manitoba Inc.</option>
                    <option value="803" style="width:146px">Latvian Credit Union Limited</option>
                    <option value="039" style="width:146px">Laurentian Bank of Canada</option>
                    <option value="522" style="width:146px">Laurentian Trust of Canada Inc.</option>
                    <option value="357" style="width:146px">M&amp;T Bank</option>
                    <option value="540" style="width:146px">Manulife Bank of Canada</option>
                    <option value="626" style="width:146px">Manulife Trust Company</option>
                    <option value="336" style="width:146px">Maple Bank</option>
                    <option value="269" style="width:146px">Mega International Commercial Bank (Canada)</option>
                    <option value="837" style="width:146px">Meridian Credit Union</option>
                    <option value="277" style="width:146px">Mizuho Corporate Bank Ltd. Canada Branch</option>
                    <option value="361" style="width:146px">MonCana Bank of Canada</option>
                    <option value="550" style="width:146px">Montreal Trust Company of Canada</option>
                    <option value="006" style="width:146px">National Bank of Canada</option>
                    <option value="590" style="width:146px">National Trust Company</option>
                    <option value="846" style="width:146px">Ontario Civil Service Credit Union Limited</option>
                    <option value="334" style="width:146px">Pacific &amp; Western Bank of Canada</option>
                    <option value="568" style="width:146px">Peace Hills Trust Company</option>
                    <option value="621" style="width:146px">Peoples Trust Company</option>
                    <option value="326" style="width:146px">President’s Choice Financial</option>
                    <option value="322" style="width:146px">Rabobank Nederland</option>
                    <option value="620" style="width:146px">ResMor Trust Company</option>
                    <option value="592" style="width:146px">Royal Bank Mortgage Corporation</option>
                    <option value="003" style="width:146px">Royal Bank of Canada</option>
                    <option value="240" style="width:146px">Royal Bank of Scotland N.V. (Canada Branch)</option>
                    <option value="570" style="width:146px">Royal Trust Company</option>
                    <option value="580" style="width:146px">Royal Trust Corporation of Canada</option>
                    <option value="606" style="width:146px">Scotia Mortgage Corporation</option>
                    <option value="002" style="width:146px">Scotiabank (The Bank of Nova Scotia)</option>
                    <option value="355" style="width:146px">Shinhan Bank Canada</option>
                    <option value="292" style="width:146px">Société Générale (Canada Branch)</option>
                    <option value="346" style="width:146px">Société Générale (Canada Branch) Ontario</option>
                    <option value="294" style="width:146px">State Bank of India (Canada) Alberta</option>
                    <option value="327" style="width:146px">State Street</option>
                    <option value="301" style="width:146px">Sumitomo Mitsui Banking Corporation of Canada</option>
                    <option value="551" style="width:146px">Sun Life Financial Trust Inc.</option>
                    <option value="597" style="width:146px">TD Mortgage Corporation</option>
                    <option value="603" style="width:146px">TD Pacific Mortgage Corporation</option>
                    <option value="242" style="width:146px">The Bank of New York Mellon</option>
                    <option value="509" style="width:146px">The Canada Trust Company</option>
                    <option value="623" style="width:146px">The Equitable Trust Company</option>
                    <option value="349" style="width:146px">The Northern Trust Company Canada Branch</option>
                    <option value="004" style="width:146px">The Toronto-Dominion Bank</option>
                    <option value="318" style="width:146px">U.S. Bank National Association</option>
                    <option value="339" style="width:146px">UBS AG Canada Branch</option>
                    <option value="290" style="width:146px">UBS Bank (Canada)</option>
                    <option value="335" style="width:146px">United Overseas Bank Limited</option>
                    <option value="359" style="width:146px">Walmart Canada Bank</option>
                </select>
            </p>',
            'account-type' => '<p class="form-row form-row-wide">
            <label for="' . $this->id . '-account-type">' . __("Account Type", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
            <select id="' . $this->id . '-account-type"  name="' . $this->id . '-account-type" 
                style="
                    font-size: 1.5em;
                    padding: 8px;
                    background-repeat: no-repeat;
                    background-position: right .618em center;
                    background-size: 32px 20px;
                    border-color: #c7c1c6;
                    border-top-color: #bbb3b9;
                    width: 100%;
                    background: #fff;
                "
                >
                <option value="C">Checking</option>
                <option value="S">Savings</option>
            </select>
			</p>',
            'transit-no-field' => '
            <p class="form-row form-row-wide">
			<label for="' . $this->id . '-transit-no">' . __("Transit Number", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
			<input onblur="this.value = this.value.replace(/[^0-9]/g, \'\');" id="' . $this->id . '-transit-no" class="input-text" type="number" min="0" max="99999" maxlength="5" autocomplete="off" name="' . $this->id . '-transit-no"  />
			</p>',
            'account-no-field' => '<p class="form-row form-row-wide">
			<label for="' . $this->id . '-account-no">' . __("Account Number", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
			<input onblur="this.value = this.value.replace(/[^0-9]/g, \'\');" id="' . $this->id . '-account-no" class="input-text " type="number" min="0" max="99999999999999999999" maxlength="20" autocomplete="off" name="' . $this->id . '-account-no" />
			</p>',
        ];

        $html = '<fieldset id="' . $this->id . '-cc-form">'
                . $fields['style']
                . $fields['institution-code']
                . $fields['account-type']
                . $fields['transit-no-field']
                . $fields['account-no-field']
                . $fields['js']
                . '<div class="clear"></div>'
                . '</fieldset>';

        echo $html;
    }

    /**
     * Check if SSL is enabled and notify the user
     */
    public function admin_notices()
    {
        if ('no' === $this->enabled) {
            return;
        }
    }
    
    /**
     * Check if this gateway is enabled
     */
    public function is_available()
    {
        if ('yes' === $this->enabled) {
            if (!$this->username || !$this->password) {
                return false;
            } else if (get_woocommerce_currency() != 'CAD') {
                return false;
            }
            return true;
        }
        return false;
    }

    public function validate_password_field($key, $value = NULL)
    {
        $post_data = $_POST;

        if ($value == NULL) {
            $value = $post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_' . $key];
        }
        if (isset($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']) && empty($value)) {
            //not validated
            WC_Admin_Settings::add_error(__('Error: You must enter a Gateway API username/password.', GatewayInterface::PLUGIN_ID));

            return false;
        } else {
            API::set_username($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']);
            API::set_password($value);
        }
        return $value;
    }

    /**
     * Validate checkout form fields
     */
    public function validate_fields()
    {
        $transitNo = isset($_POST[$this->id . '-transit-no']) ? wc_clean($_POST[$this->id . '-transit-no']) : '';
        $accountNo = isset($_POST[$this->id . '-account-no']) ? wc_clean($_POST[$this->id . '-account-no']) : '';

        $institutionCode = isset($_POST[$this->id . '-institution-code']) ? wc_clean($_POST[$this->id . '-institution-code']) : '';
        $accountType = isset($_POST[$this->id . '-account-type']) ? wc_clean($_POST[$this->id . '-account-type']) : '';
        // Validate $institutionCode
        if (empty($institutionCode)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Institution Code required.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Institution Code required.', GatewayInterface::PLUGIN_ID));
        }  
        
        // Validate $accountType
        if (empty($accountType)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Account Type required.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Account Type required.', GatewayInterface::PLUGIN_ID));
        } 

        // Validate $transitNo
        if (empty($transitNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Transit Number must be provided.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Transit Number must be provided.', GatewayInterface::PLUGIN_ID));
        } elseif (!preg_match('/^\d{5}$/', $transitNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Transit Number must be numbers only and 5 digits mandatory.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Transit Number must be numbers only and 5 digits mandatory.', GatewayInterface::PLUGIN_ID));
        }

        // Validate $accountNo
        if (empty($accountNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Account Number must be provided.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Account Number must be provided.', GatewayInterface::PLUGIN_ID));
        } elseif (!preg_match('/\d/', $accountNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Account Number must be numbers only.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Account Number must be numbers only.', GatewayInterface::PLUGIN_ID));
        }
    }

    /**
     * Process the payment
     *
     * @param int  $order_id Reference.
     * @param bool $retry Should we retry on fail.
     * @param bool $force_customer Force user creation.
     *
     * @throws Exception If payment will not be accepted.
     *
     * @return mixed
     */
    public function process_payment($order_id, $retry = true, $force_customer = false)
    {
        ini_set('display_errors', 'Off'); //notices breaking json

        /**
         * @var $order WC_Order
         */
        $order = wc_get_order($order_id);

        // If order free, do nothing.
        if ($order->get_total() == 0) {
            $order->payment_complete();

            // Return thank you page redirect.
            return [
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            ];
        }
        $request = API::prepare_data($order, $_POST);
        $response = API::request($request);
        self::log('GW_API_RESPONSE: ' . var_export($response, true));

        switch ($response->{'ResponseCode'}) {
            case 1: // Approved
                add_post_meta($order->id, '_transaction_id', $response->{'Reference'}, true);
                $order->add_order_note(sprintf(__(GatewayInterface::PLUGIN_TITLE . ' charge complete (Transaction ID: %s)', 'woocommerce-gateway-stripe'), $response->{'Reference'}));
                $order->update_status('wc-processing');

                // Remove cart.
                WC()->cart->empty_cart();

                // Return thank you page redirect.
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
                break;
            case 5: // Approved
                add_post_meta($order->id, '_transaction_id', $response->{'Reference'}, true);
                $order->add_order_note(sprintf(__(GatewayInterface::PLUGIN_TITLE . ' charge complete (Transaction ID: %s)', 'woocommerce-gateway-stripe'), $response->{'Reference'}));
                $order->update_status('wc-processing');

                // Remove cart.
                WC()->cart->empty_cart();

                // Return thank you page redirect.
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
                break;

            default: // Declined / Failed
                break;
        }
    }

}
