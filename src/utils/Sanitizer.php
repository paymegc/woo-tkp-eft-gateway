<?php

namespace WooTkpEFTGateway\utils;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Sanitizer
 * 
 * This class provides functions which serves on sanitization of desired strings
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Sanitizer
{

    /*
     * Holds the matching strings
     * 
     * @var array
     */
    protected static $arrPossibleVariables = [
        'transit-no',
        'password'
    ];

    /**
     * Sanitize sensitive data from request
     * 
     * @param array|object $request
     * @param boolean $sanitizerOn
     * @return mixed
     */
    public static function sanitizeRequest($request, $sanitizerOn = true)
    {
        switch (gettype($request)) {

            case 'array':

                $tmpObj = $request;

                foreach ($tmpObj as $key => $value) {
                    if (is_array($value) || is_object($value)) {
                        $tmpObj[$key] = self::sanitizeRequest($value);
                    } else {
                        $tmpObj[$key] = self::sanitizeString($key, $value);
                    }
                }

                break;

            case 'object':

                $tmpObj = clone $request;

                foreach ($tmpObj as $key => $value) {
                    if (is_object($value) || is_array($value)) {
                        $tmpObj->{$key} = self::sanitizeRequest($value);
                    } else {
                        $tmpObj->{$key} = self::sanitizeString($key, $value);
                    }
                }

                break;
        }

        return $tmpObj;
    }

    /**
     * Sanitize a given string
     * 
     * @param string|integer $key
     * @param string $value
     * @return string
     */
    private static function sanitizeString($key, $value)
    {
        if (preg_match('/\d{10,}/', $value)) {
            return 'XXXX - ' . substr($value, -4, 4);
        }
        if (preg_match('/' . implode('|', self::$arrPossibleVariables) . '/i', $key)) {
            return '****';
        }
        return $value;
    }

}
