<?php

namespace WooTkpEFTGateway\admin;

use WooTkpEFTGateway\API;
use WooTkpEFTGateway\Gateway;
use WooTkpEFTGateway\GatewayInterface;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       https://teknepay.com
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Controller
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        add_action('plugins_loaded', [$this, 'init']);
        add_action('admin_init', [$this, 'check_environment']);
        add_action('admin_notices', [$this, 'admin_notices']);
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in PluginName_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The PluginName_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/woo-tkp-eft-gateway-admin.css', [], $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in PluginName_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The PluginName_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/woo-tkp-eft-gateway-admin.js', ['jquery'], $this->version, false);
    }

    /**
     * Initialize the gateway. Called very early - in the context of the plugins_loaded action
     *
     * @since 1.0.0
     */
    public function init_gateways()
    {
        if (!class_exists('WC_Payment_Gateway')) {
            return;
        }

        add_filter('woocommerce_payment_gateways', [$this, 'add_gateways']);
    }

    public function init()
    {
        // Init the gateway itself
        $this->init_gateways();

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), [$this, 'plugin_action_links']);
    }

    /**
     * Adds plugin action links
     *
     * @since 1.0.0
     */
    public function plugin_action_links($links)
    {
        $plugin_links = [
            '<a href="' . $this->get_setting_link() . '">' . __('Settings', GatewayInterface::PLUGIN_ID) . '</a>'
        ];

        return array_merge($plugin_links, $links);
    }

    /**
     * Get setting link.
     *
     * @since 1.0.0
     *
     * @return string Setting link
     */
    public function get_setting_link()
    {
        return admin_url('admin.php?page=wc-settings&tab=checkout&section=' . GatewayInterface::PLUGIN_ID);
    }

    /**
     * Add the gateways to WooCommerce
     *
     * @since 1.0.0
     */
    public function add_gateways($methods)
    {
        $methods[] = Gateway::class;

        return $methods;
    }

    /**
     * Display any notices we've collected thus far (e.g. for connection, disconnection)
     */
    public function admin_notices()
    {
        foreach ((array) $this->notices as $notice) {
            echo "<div class='" . esc_attr($notice['class']) . "'><p>";
            echo wp_kses($notice['message'], ['a' => ['href' => []]]);
            echo "</p></div>";
        }
    }

    /**
     * Allow this class and other classes to add slug keyed notices (to avoid duplication)
     */
    public function add_admin_notice($slug, $class, $message)
    {
        $this->notices[$slug] = [
            'class' => $class,
            'message' => $message
        ];
    }

    /**
     * The backup sanity check, in case the plugin is activated in a weird way,
     * or the environment changes after activation.
     */
    public function check_environment()
    {
        $environment_warning = self::get_environment_warning();

        $pluginActive = preg_grep('/' . GatewayInterface::PLUGIN_ID . '/', apply_filters('active_plugins', get_option('active_plugins')));

        if ($environment_warning && count($pluginActive)) {
            $this->add_admin_notice('bad_environment', 'error', $environment_warning);
        }

        // Check if secret key present. Otherwise prompt, via notice, to go to setting.
        $username = API::get_username();
        $password = API::get_password();

        if ((empty($username) || empty($password)) &&
                !( isset($_GET['page'], $_GET['section']) &&
                'wc-settings' === $_GET['page'] &&
                GatewayInterface::PLUGIN_ID === $_GET['section'] )) {
            $setting_link = $this->get_setting_link();
            $this->add_admin_notice('prompt_connect'
                    , 'notice notice-warning'
                    , sprintf(__(GatewayInterface::PLUGIN_TITLE . ' is almost ready. To get started, <a href="%s">set your api credentials</a>.'
                                    , GatewayInterface::PLUGIN_ID)
                            , $setting_link));
        }
    }

    /**
     * Checks the environment for compatibility problems.  Returns a string with the first incompatibility
     * found or false if the environment has no problems.
     */
    public static function get_environment_warning()
    {
        if (!defined('WC_VERSION')) {
            return __('WooCommerce ' . GatewayInterface::PLUGIN_TITLE . ' requires WooCommerce to be activated to work.'
                    , GatewayInterface::PLUGIN_ID);
        }

        if (version_compare(WC_VERSION, GatewayInterface::PLUGIN_MIN_WC_VER, '<')) {
            $message = __('WooCommerce ' . GatewayInterface::PLUGIN_TITLE . ' - The minimum WooCommerce version required for this plugin is %1$s. You are running %2$s.'
                    , GatewayInterface::PLUGIN_ID
                    , GatewayInterface::PLUGIN_ID);

            return sprintf($message, GatewayInterface::PLUGIN_MIN_WC_VER, WC_VERSION);
        }

        if (!function_exists('curl_init')) {
            return __('WooCommerce ' . GatewayInterface::PLUGIN_TITLE . ' - cURL is not installed.'
                    , GatewayInterface::PLUGIN_ID);
        }

        if (get_woocommerce_currency() != 'CAD') {
            return __('WooCommerce ' . GatewayInterface::PLUGIN_TITLE . ' - Only available for Canada, unsupported currency detected.');
        }

        return false;
    }

}
