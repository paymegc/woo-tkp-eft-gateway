<?php

namespace WooTkpEFTGateway\utils;

use WooTkpEFTGateway\GatewayInterface;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Fired during plugin uninstallation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Uninstaller
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function uninstall()
    {
        // If uninstall not called from WordPress, then exit.
        if (!defined('WP_UNINSTALL_PLUGIN')) {
            exit;
        }

        $option_name = 'woocommerce_' . GatewayInterface::PLUGIN_ID . '_settings';

        delete_option($option_name);

        // for site options in Multisite
        delete_site_option($option_name);
    }

}
