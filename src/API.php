<?php

namespace WooTkpEFTGateway;

use WooTkpEFTGateway\utils\HelpersTrait;
use WooTkpEFTGateway\utils\Sanitizer;
use WP_Error;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * API
 *
 * This class provides holds the API functionality
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class API
{

    use HelpersTrait;

    /**
     * Secret API Username
     * 
     * @var string
     */
    private static $username = '';

    /**
     * Secret API Password
     * 
     * @var string
     */
    private static $password = '';

    /**
     * The profile ID assigned by the gateway
     * 
     * @var int 
     */
    private static $profile_id;

    /**
     * Array of settings
     * 
     * @var array 
     */
    private static $settings = [
        'gw_api_url' => GatewayInterface::GW_API_URL,
        'app_domain' => GatewayInterface::APP_DOMAIN
    ];

    /**
     * Get the default or assigned settings
     * 
     * @param $setting
     * @return mixed|null
     */
    public static function getSetting($setting)
    {
        $settings = self::$settings;

        if (isset($settings[$setting])) {
            return $settings[$setting];
        }

        return null;
    }

    /**
     * Set the API url
     * 
     * @param string $gw_api_url
     */
    public static function set_gw_api_url($gw_api_url)
    {
        self::$settings['gw_api_url'] = $gw_api_url;
    }

    /**
     * Get the API url
     * 
     * @return string
     */
    public static function get_gw_api_url()
    {
        $options = get_option('woocommerce_' . GatewayInterface::PLUGIN_ID . '_settings');

        if (isset($options['gw_api_url']) && trim($options['gw_api_url']) != '') {
            self::set_gw_api_url($options['gw_api_url']);
        }

        return self::$settings['gw_api_url'];
    }

    /**
     * Set the API username
     * 
     * @param string $username
     */
    public static function set_username($username)
    {
        self::$username = $username;
    }

    /**
     * Get the API username
     * 
     * @return string
     */
    public static function get_username()
    {
        if (!self::$username) {
            $options = get_option('woocommerce_' . GatewayInterface::PLUGIN_ID . '_settings');

            if (isset($options['gw_api_username'])) {
                self::set_username($options['gw_api_username']);
            }
        }

        return self::$username;
    }

    /**
     * Set the API password
     * 
     * @param string $password
     */
    public static function set_password($password)
    {
        self::$password = $password;
    }

    /**
     * Get the API password
     * 
     * @return string
     */
    public static function get_password()
    {
        if (!self::$password) {
            $options = get_option('woocommerce_' . GatewayInterface::PLUGIN_ID . '_settings');

            if (isset($options['gw_api_password'])) {
                self::set_password($options['gw_api_password']);
            }
        }
        return self::$password;
    }

    /**
     * Set the profile ID
     * 
     * @param string $profile_id
     */
    public static function set_profile_id($profile_id)
    {
        self::$profile_id = $profile_id;
    }

    /**
     * Get the profile ID
     * 
     * @return int
     */
    public static function get_profile_id()
    {
        if (!self::$profile_id) {
            $options = get_option('woocommerce_' . GatewayInterface::PLUGIN_ID . '_settings');

            if (isset($options['gw_profile_id'])) {
                self::set_profile_id($options['gw_profile_id']);
            }
        }

        return self::$profile_id;
    }

    /**
     * Prepare data for request
     * 
     * @param string $order
     * @param array $post_data
     * @return array
     */
    public static function prepare_data($order, array $post_data)
    {
        $order = json_decode($order);
        
        $profile_id = self::get_profile_id();
        $password = self::get_password();
        $customer_trans_id = $order->id;
        $amount = number_format($order->total, 2, '.', '');
        
        return [
            "authenticate" => ['user' => self::get_username(), 'password' => $password],
            "transaction" => [
                "ProfileID" => $profile_id,
                "CustomerTransID" => $customer_trans_id,
                "CustomerID" => $order->customer_id ? $order->customer_id : rand(10000, 99999),
                "FirstName" => ucfirst($order->billing->first_name),
                "LastName" => ucfirst($order->billing->last_name),
                "Action" => "Sale",
                "Country" => strtoupper($order->billing->country),
                "Address" => $order->billing->address_1 . ($order->billing->address_2 ? ' ' . $order->billing->address_2 : null),
                "City" => $order->billing->city,
                "State" => strtoupper($order->billing->state),
                "PostalCode" => $order->billing->postcode,
                "Email" => $order->billing->email,
                "Phone" => $post_data['billing_phone'],
                "Amount" => $amount,
                "Currency" => strtoupper($order->currency),
                "BankName" => 'N/A',
                "PaymentType" => "EFT",
                "RoutingNumber" => preg_replace('/\s+/', '', $post_data[GatewayInterface::PLUGIN_ID . '-transit-no'])."-".preg_replace('/\s+/', '', $post_data[GatewayInterface::PLUGIN_ID . '-institution-code']),
                "AccountNumber" => preg_replace('/\s+/', '', $post_data[GatewayInterface::PLUGIN_ID . '-account-no']),
                "AccountType" => preg_replace('/\s+/', '', $post_data[GatewayInterface::PLUGIN_ID . '-account-type']),
                "TransType" => "DT"
            ]
        ];
    }

    public function get_url()
    { 
        return (WC()->payment_gateways->payment_gateways()[GatewayInterface::PLUGIN_ID]->settings["gw_api_url"]);
    }
    /**
     * Send the request to plugin's API
     *
     * @param mixed $request
     * @return array|WP_Error
     */
    public static function request($request, $method = 'POST')
    {
        $url = self::get_url();
        test_f_2($url);
        self::log("GW_API_REQUEST: " . $url . "\r\n" . print_r(Sanitizer::sanitizeRequest($request), true));
        
        $response = wp_remote_post($url
                , [
            'method' => $method,
            'body' => apply_filters('woocommerce_' . GatewayInterface::PLUGIN_ID . '_request_body', $request),
            'timeout' => 70,
            'user-agent' => 'WooCommerce ' . WC()->version,
            'x-domain' => home_url('/')
                ]
        );

        $parsed_response = json_decode($response['body']);
        
        if (empty($response['body'])) {
            self::log("Error Response: " . print_r($response, true));
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('There was a problem connecting to the payment gateway.' . $parsed_response->name, GatewayInterface::PLUGIN_ID));
        }

        if (is_wp_error($response)) {
            self::log("Error Response: " . print_r($response, true));
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('There was a problem connecting to the payment gateway.' . $parsed_response->name, GatewayInterface::PLUGIN_ID));
        }

        return $parsed_response;
    }
    
}
