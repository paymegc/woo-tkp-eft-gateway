<?php

namespace WooTkpEFTGateway\utils;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        
    }

}
