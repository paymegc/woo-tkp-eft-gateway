<?php

namespace WooTkpEFTGateway\config;

use WooTkpEFTGateway\GatewayInterface;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Settings class
 * 
 * @link       https://teknepay.com
 * @since      1.0.0
 */
class Settings
{

    public static function getSettings()
    {
        return apply_filters('wc_' . GatewayInterface::PLUGIN_ID . '_settings', [
            'enabled' => [
                'title' => __('Enable/Disable', GatewayInterface::PLUGIN_ID),
                'label' => __('Enable', GatewayInterface::PLUGIN_ID),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no',
            ],
            'title' => [
                'title' => __('Title', GatewayInterface::PLUGIN_ID),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', GatewayInterface::PLUGIN_ID),
                'default' => __('EFT Payment Gateway', GatewayInterface::PLUGIN_ID),
                'desc_tip' => true,
            ],
            'description' => [
                'title' => __('Description', GatewayInterface::PLUGIN_ID),
                'type' => 'text',
                'description' => __('This controls the description which the user sees during checkout.', GatewayInterface::PLUGIN_ID),
                'default' => __('Pay using EFT payment method.', GatewayInterface::PLUGIN_ID),
                'desc_tip' => true,
            ],
            'gw_api_url' => [
                'title' => __('Gateway API URL', GatewayInterface::PLUGIN_ID),
                'type' => 'text',
                'description' => __('Gateway API URL provided by TeknePay.', GatewayInterface::PLUGIN_ID),
                'default' => __(GatewayInterface::GW_API_URL, GatewayInterface::PLUGIN_ID),
                'desc_tip' => true,
            ],
            'gw_api_username' => [
                'title' => __('Gateway API Username', GatewayInterface::PLUGIN_ID),
                'type' => 'text',
                'description' => __('Get your API username from TeknePay (https://teknepay.com).', GatewayInterface::PLUGIN_ID),
                'default' => '',
                'desc_tip' => true,
            ],
            'gw_api_password' => [
                'title' => __('Gateway API Password', GatewayInterface::PLUGIN_ID),
                'type' => 'password',
                'description' => __('Get your API password from TeknePay (https://teknepay.com).', GatewayInterface::PLUGIN_ID),
                'default' => '',
                'desc_tip' => true,
            ],
            'gw_profile_id' => [
                'title' => __('Profile ID', GatewayInterface::PLUGIN_ID),
                'type' => 'text',
                'description' => __('Get your Profile ID from TeknePay (https://teknepay.com).', GatewayInterface::PLUGIN_ID),
                'default' => '',
                'desc_tip' => true,
            ],
        ]);
    }

}
