<?php

namespace WooTkpEFTGateway\utils;

use WC_Logger;
use WooTkpEFTGateway\GatewayInterface;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * HelpersTrait.
 *
 * This class defines plugin's helper functions.
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
trait HelpersTrait
{

    /**
     * @var Reference to logger class.
     */
    private static $log;

    /**
     * Log messages
     * 
     * @param string $message
     */
    public static function log($message)
    {
        if (!self::$log) {
            self::$log = new WC_Logger();
        }
         
        self::$log->add(GatewayInterface::PLUGIN_ID, $message);

        if (defined('WP_DEBUG') && WP_DEBUG) {
            error_log($message);
        }
    }

}
