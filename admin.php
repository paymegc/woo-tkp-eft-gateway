<?php

namespace WooTkpEFTGateway;

use WooTkpEFTGateway\utils\Activator;
use WooTkpEFTGateway\utils\Deactivator;
use WooTkpEFTGateway\utils\Uninstaller;

// If this file is called directly, abort.
defined('ABSPATH') or exit;
defined('WPINC') or die;

// We load Composer's autoload file
require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

/**
 * The code that runs during plugin activation.
 */
function activate_woo_tkp_eft_gateway()
{
    if (get_woocommerce_currency() != 'CAD') {
        die('WooCommerce ' . GatewayInterface::PLUGIN_TITLE . ' - Only available for Canada, unsupported currency detected.');
    }
    
    Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_woo_tkp_eft_gateway()
{
    Deactivator::deactivate();
}

/**
 * The code that runs during plugin uninstallation.
 */
function uninstall_woo_tkp_eft_gateway()
{
    Uninstaller::uninstall();
}

register_activation_hook(__FILE__, '\\' . __NAMESPACE__ . '\activate_woo_tkp_eft_gateway');
register_deactivation_hook(__FILE__, '\\' . __NAMESPACE__ . '\deactivate_woo_tkp_eft_gateway');
register_uninstall_hook(__FILE__, '\\' . __NAMESPACE__ . '\uninstall_woo_tkp_eft_gateway');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_woo_tkp_eft_gateway()
{
    $plugin = new Main();
    $plugin->run();
}

run_woo_tkp_eft_gateway();
