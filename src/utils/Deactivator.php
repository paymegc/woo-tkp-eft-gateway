<?php

namespace WooTkpEFTGateway\utils;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Deactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {
        
    }

}
